var socket;
socket = io.connect('http://172.20.12.187:2222');

msgIter = false;
name = {};
currentRoom = 1;

socket.on('newMessage', function(data){
  if(data[2] == currentRoom){
    msgIter = !msgIter;
    area = document.getElementById('messages');

    area.innerHTML = area.innerHTML + '<div id=msg_' + msgIter.toString() +  '> ' + data[0] + ": " + data[1] + "</div>";
    scrollObj =document.getElementById("msgArea");
    scrollObj.scrollTop = scrollObj.scrollHeight;
  }
})

socket.on('accessGranted', function(data){

  oldRoom = currentRoom;
  currentRoom = data;

  stuff = {

    old : oldRoom,
    new : currentRoom

  }
  
  socket.emit('changedRoom', stuff);

  area = document.getElementById('messages');
  area.innerHTML = "<div style=\"text-align: center\">-- ACCESS GRANTED --</div>";

  oldButton = document.getElementById('chat' + oldRoom + 'Button');
  oldButton.style.fontSize = '3vw';
  oldButton.style.color = "white";


})

socket.on('accessDenied', function(){

  area = document.getElementById('messages');
  area.innerHTML = area.innerHTML + "<div style=\"text-align: center\">-- ACCESS DENIED --</div>";

})

socket.on('movedRoom', function(data){

  area = document.getElementById('messages');
  if(data.old == currentRoom){
    area.innerHTML = area.innerHTML + "<div> Someone has left your room. </div>"
  } else if (data.new == currentRoom) {
    area.innerHTML = area.innerHTML + "<div> Someone has joined your room. </div>"
  }

})

console.log('working');

function sendMessage(){
  msgIter = !msgIter;
  msg = document.getElementById("msgInput");

  area = document.getElementById('messages');
  message = [name, msg.value, currentRoom];


  area.innerHTML = area.innerHTML + '<div id=msg_' + msgIter.toString() +  '> ' + message[0] + ": " + message[1] + "</div>";
  scrollObj =document.getElementById("msgArea");
  scrollObj.scrollTop = scrollObj.scrollHeight;

  socket.emit('message', message);
  msg.value = "";
}

function imageBox(){
  box = document.getElementById("imageArea");
  box.style.display = "initial";
}

function sendImage(){
  imgURL = document.getElementById("imgInput");
  message = [name, imgURL.value, currentRoom];
  area = document.getElementById('messages');

  area.innerHTML = area.innerHTML + '<div> ' + message[0] + ': ' + '<a href=\" ' + message[1] + '\" target=\"_blank\"><img id = \"imgBox\" src=' + message[1] + '></a>' + "</div>";

  scrollObj=document.getElementById("msgArea");
  scrollObj.scrollTop = scrollObj.scrollHeight;
  socket.emit('image', message);
  imgURL.value = "";
  document.getElementById("imageArea").style.display = "none";
}

socket.on('newImage', function(message){
  if(message[2] == currentRoom){
    area = document.getElementById('messages');
    area.innerHTML = area.innerHTML + '<div> ' + message[0] + ': ' + '<img id = \"imgBox\" src=' + message[1] + '>' + " " + "</div>";

    scrollObj=document.getElementById("msgArea");
    scrollObj.scrollTop = scrollObj.scrollHeight;
  }
})

function setUsername(){
  
  name = document.getElementById("nameBox").value;
  document.getElementById("sidebar").style.display = "initial";
  document.getElementById("chatArea").style.display = "initial";
  document.getElementById("startTitle").style.display = "none";
  document.getElementById("nameBox").style.display = "none";
  document.cookie = name;

}

function activeRoom(room, old=false){

  button = document.getElementById('chat' + room + 'Button');
  button.style.fontSize = '5vw';
  button.style.color = "orange";
  if(old != false){
    oldButton = document.getElementById('chat' + old + 'Button');
    oldButton.style.fontSize = '3vw';
    oldButton.style.color = "white";
  }

}

function changeRoom(val){
  if(currentRoom != val){
    oldRoom = currentRoom;
    currentRoom = val;
    data = {

      old : oldRoom,
      new : currentRoom

    }
    area = document.getElementById('messages');
    area.innerHTML = "<div style=\"text-align: center\"> -- CHANGED TO CHATROOM " + val + " -- </div>";
    socket.emit('changedRoom', data);
    activeRoom(currentRoom, oldRoom);
  } else {
    area = document.getElementById('messages');
    area.innerHTML = area.innerHTML + "<div>You are already in that room...</div>"
  }

}

function joinPasswordRoom(){

  document.getElementById('passwordArea').style.display = "initial";

}

function sendPassword(){

  password = document.getElementById('passwordInput');
  socket.emit('accessRoom8', password.value);
  password.value = "";
  console.log('doing');
  document.getElementById('passwordArea').style.display = "none";


}


window.onload = function(){

  if(document.cookie != ""){
    document.getElementById("sidebar").style.display = "initial";
    document.getElementById("chatArea").style.display = "initial";
    document.getElementById("startTitle").style.display = "none";
    document.getElementById("nameBox").style.display = "none";
    name = document.cookie;
    area = document.getElementById('messages');
    area.innerHTML = area.innerHTML + '<div style=\"text-align: center\">-- CHATROOM 1 --</div>';
    activeRoom(1);
  }

}