var express = require('express');

var app = express();
var server = app.listen(2222);
password = 'dude';
room8 = 'rachelgreen';
console.log('Listening on port 2222');

app.use(express.static('public'));

var socket = require('socket.io');

var io = socket(server);

io.sockets.on('connection', newConnection);

function newConnection(socket){
  console.log('New Connection: ' + socket.id);

  socket.on('message', function(data){
    console.log("CHATROOM " + data[2] + " - " + data[0] + ": " + data[1]);
    socket.broadcast.emit('newMessage', data)
  })

  socket.on('image', function(data){

    console.log("Chatroom " + data[2] +' - Image from: ' + data[0]);
    socket.broadcast.emit('newImage', data);

  })

  socket.on('changedRoom', function(data){

    console.log('Someone moved from room ' + data.old + " to room " + data.new);
    socket.broadcast.emit('movedRoom', data);

  })

  socket.on('accessRoom8', function(data){

    console.log('Trying to access private room');
    if(data == password){
      socket.emit('accessGranted', room8);
      console.log('yes');
    } else {
      socket.emit('accessDenied');
    }

  })

}
